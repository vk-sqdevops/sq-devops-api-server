output "api-alb-dns" {
  value = "${aws_instance.api-server.public_dns}"
}
