# Creds & Region
variable "aws-access-key" {}
variable "aws-secret-key" {}
variable "aws-region" {}

# Vpc Id
variable "vpc-id" {}

# Subnets
variable "subnets" {
  type="map"
}

# Security Group
variable "web-server-sg-id" {}

# Naming and tagging
variable "deployment-prefix" {}
variable "organization" {}
variable "project" {}
variable "environment" {}

# Web Server Desktop
variable "api-server-ami-id" {}
variable "api-server-instance-count" {}
variable "api-server-instance-type" {}
variable "api-server-key-pair" {}
variable "ec2-instance-profile-name" {}

# S3 Bucket
variable "s3-bucket-name" {}
variable "s3-settings-path" { 
 default = "settings.json"
}
